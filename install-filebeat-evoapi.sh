wget https://ops-aiops.oss-cn-hongkong.aliyuncs.com/package/filebeat-7.10.1-linux-x86_64.tar.gz
tar zxvf filebeat-7.10.1-linux-x86_64.tar.gz -C /usr/local/
ln -s /usr/local/filebeat-7.10.1-linux-x86_64 /usr/local/filebeat
cd /usr/local/filebeat
rm -rf filebeat.yml

cat>/usr/local/filebeat/filebeat.yml<<\EOF
filebeat.inputs:
- type: log
  enabled: true
  paths:
    - /home/wwwlog/api6/api6.log
  fields:
      system: evo_prd_api6_log
  tail_files: true
  multiline.pattern: '^\d{4}-\d{1,2}-\d{1,2}'
  multiline.negate: true
  multiline.match: after
- type: log
  enabled: true
  paths:
    - /home/wwwlog/api6/api6.log
  fields:
      system: evo2_prd_api6_log
  multiline.pattern: '^\d{4}-\d{1,2}-\d{1,2}'
  multiline.negate: true
  multiline.match: after
- type: log
  enabled: true
  paths:
    - /home/wwwlog/ptjapi6/catalina.out
  fields:
      system: evo_prd_ptj_log
  tail_files: true
  multiline.pattern: '^(20|21|22|23|[0-1]\d):[0-5]\d:[0-5]\d'
  multiline.negate: true
  multiline.match: after
- type: log
  enabled: true
  paths:
    - /home/wwwlog/ptjapi6/localhost_access_log*
  fields:
      system: evo_prd_ptj_access_log
  tail_files: true
max_procs: 3
output.kafka:
  enabled: true
  hosts: ["172.16.1.225:9092","172.16.1.226:9092","172.16.1.227:9092"]
  topic: '%{[fields][system]}'
  max_message_bytes: 10000000
  compression: gzip
EOF

#自定义启动脚本
cat>/usr/lib/systemd/system/filebeat.service<<\EOF
[Unit]
Description=filebeat
Documentation=https://www.elastic.co/guide/en/beats/filebeat/current/index.html
Wants=network-online.target
After=network-online.target

[Service]
User=root
ExecStart=/usr/local/filebeat/filebeat -c /usr/local/filebeat/filebeat.yml
Restart=always

[Install]
WantedBy=multi-user.target
EOF

chmod +x /usr/lib/systemd/system/filebeat.service

#启动进程
systemctl enable filebeat
systemctl start filebeat
ps -ef|grep -v grep|grep filebeat

#计划任务
cat>>/var/spool/cron/root<<\EOF

#清理filebeat日志
00 13 * * * [ "-e /usr/local/filebeat/logs/filebeat.[1-9]" ] && rm -f /usr/local/filebeat/logs/filebeat.*
EOF

echo -e '
     rm -f filebeat-7.10.1-linux-x86_64.tar.gz install-filebeat-evoapi.sh
     '
