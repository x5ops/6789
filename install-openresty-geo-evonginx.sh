#!/bin/bash
[ -e /data/soft ] || mkdir -p /data/soft
cd /data/soft
ngversion="1.19.9.1"
yum install gcc -y
yum install libmaxminddb-devel pcre-devel openssl-devel gcc postgresql-devel -y
yum -y install gcc gcc-c++ autoconf automake make git
yum -y install zlib zlib-devel openssl openssl-devel pcre pcre-devel
yum -y install GeoIP GeoIP-devel GeoIP-data

wget https://openresty.org/download/openresty-${ngversion}.tar.gz
tar zxvf openresty-${ngversion}.tar.gz

wget https://github.com/maxmind/libmaxminddb/releases/download/1.3.2/libmaxminddb-1.3.2.tar.gz
tar -zxvf libmaxminddb-1.3.2.tar.gz
cd libmaxminddb-1.3.2
./configure && make && make install
echo /usr/local/lib  >> /etc/ld.so.conf.d/local.conf 
ldconfig

cd ..

git clone https://github.com/ar414-com/nginx-geoip2


cd openresty-${ngversion}

./configure --prefix=/usr/local/openresty \
--with-luajit \
--without-http_redis2_module \
--with-http_iconv_module \
--with-http_postgres_module \
--with-http_stub_status_module \
--with-http_ssl_module \
--with-http_gzip_static_module \
--with-http_geoip_module \
--with-http_realip_module \
--add-module=../nginx-geoip2/ngx_http_geoip2_module


make -j 8 && make install 


cat >>/etc/profile<<\EOF
export PATH=$PATH:/usr/local/openresty/nginx/sbin
EOF

chmod +x /etc/rc.d/rc.local
cat >>/etc/rc.local<<\EOF
/usr/local/openresty/nginx/sbin/nginx
EOF

mkdir -p /usr/local/openresty/nginx/geoip/ 
mkdir -p /usr/local/openresty/nginx/conf/vhosts/
mkdir -p /home/wwwlog/nginx/{evoapi,evocms,evoiptv,evoptjapi,evoquartz,evoevai,evoevcms}
cd /usr/local/openresty/nginx/geoip/
wget http://geod.01766.cc/GeoLite2-Country.mmdb
wget http://geod.01766.cc/GeoLite2-ASN.mmdb
wget http://geod.01766.cc/ASN.mmdb

source /etc/profile

cp /usr/local/openresty/nginx/conf/nginx.conf /usr/local/openresty/nginx/conf/nginx.conf.old

touch /usr/local/openresty/nginx/conf/whiteiplist.conf


cat>/usr/local/openresty/nginx/conf/nginx.conf<<\EOF
user  nobody;
worker_processes auto;
worker_cpu_affinity auto;
worker_rlimit_nofile 655350;
pid        logs/nginx.pid;

events {
    use epoll;
    worker_connections  655350;
    multi_accept on;
    accept_mutex on;
}


http
    {
        include       mime.types;
        default_type  application/octet-stream;


    log_format nginx_json_format '{"@timestamp":"$time_iso8601",'
                                '"remote_addr":"$remote_addr",'
                                '"http_x_forwarded_for":"$http_x_forwarded_for",'
                                '"geoip":"$geoip2_data_country_code",'
                                '"geo_asn_code":"$geoip2_data_asn_code",'
                                '"geo_asn_name":"$geoip2_data_asn_name",'
                                '"allowed_asn":"$allowed_asn",'
                                '"scheme":"$scheme",'
                                '"server_name":"$http_host",'
                                '"server_addr":"$server_addr",'
                                '"request":"$request",'
                                '"status":"$status",'
                                '"body_bytes_sent":"$body_bytes_sent",'
                                '"req_url":"$request_uri",'
                                '"request_time":"$request_time",'
                                '"upstream_addr":"$upstream_addr",'
                                '"upstream_status":"$upstream_status",'
                                '"upstream_response_time":"$upstream_response_time",'
                                '"referer":"$http_referer",'
                                '"mac":"$http_mac",'
                                '"model":"$http_model",'
                                '"flag":"$flag",'
                                '"agent":"$http_user_agent"}';

        server_names_hash_bucket_size 512;
        client_header_buffer_size 10m;
        large_client_header_buffers 4 10m;
        client_max_body_size 100m;
        client_body_buffer_size 512k;
        proxy_connect_timeout 200;
        proxy_send_timeout 200;
        proxy_read_timeout 200;


        proxy_buffers 16 1024k;
        proxy_buffer_size 1024k;
        proxy_busy_buffers_size 1024k;

        sendfile   on;
        tcp_nopush on;
        tcp_nodelay on;
        keepalive_timeout 60;

        send_timeout 300;
        fastcgi_connect_timeout 300;
        fastcgi_send_timeout 300;
        fastcgi_read_timeout 300;
        fastcgi_buffer_size 64k;
        fastcgi_buffers 4 64k;
        fastcgi_busy_buffers_size 128k;
        fastcgi_temp_file_write_size 256k;
        proxy_ignore_client_abort on;


       #根据ip限制CDN服务器tcp,http连接数,【apk下载 控制 req = 100/s】
       limit_conn_zone $binary_remote_addr zone=conn_cdn:30m;
       limit_req_zone $binary_remote_addr zone=req_cdn:30m rate=100r/s;


       #禁止国家地区访问
        geoip2  /usr/local/openresty/nginx/geoip/GeoLite2-Country.mmdb {
        $geoip2_data_country_code source=$http_x_forwarded_for  country iso_code;
        }
        map $geoip2_data_country_code $allowed_country {
         default yes;
         CN no;
        }

       #ASN
        geoip2 /usr/local/openresty/nginx/geoip/GeoLite2-ASN.mmdb {
          $geoip2_data_asn_code default=0 source=$http_x_forwarded_for autonomous_system_number;
          $geoip2_data_asn_name default=null source=$http_x_forwarded_for autonomous_system_organization;
        }
        map $geoip2_data_asn_code $allowed_asn {
         default yes;
         include /usr/local/openresty/nginx/geoip/ASN.mmdb;
        }
######################## ip白名单 ########################
        #ip白名单
        geo $http_x_forwarded_for $whiteiplist{
           default 0;
           include whiteiplist.conf;
        }
######################## ip白名单 ########################

        variables_hash_max_size 4096;
        variables_hash_bucket_size 2048;

        gzip on;
        gzip_min_length  1k;
        gzip_buffers     4 16k;
        gzip_http_version 1.1;
        gzip_comp_level 4;
        gzip_types     text/plain application/json  application/javascript application/x-javascript text/javascript text/css application/xml application/xml+rss;
        gzip_vary on;
        gzip_proxied   expired no-cache no-store private auth;
        gzip_disable   "MSIE [1-6]\.";

        server_tokens off;
        access_log off;

include vhosts/*.conf;
}
EOF


#定时更新ip库
cat>/opt/upgeoipdb.sh<<\EOF
#!/bin/bash
[ -e /home/geotmp ]||mkdir -p /home/geotmp
cd /home/geotmp
[ ! -e ./GeoLite2-Country.mmdb ]||rm -f GeoLite2-Country.mmdb
[ ! -e ./GeoLite2-ASN.mmdb ]||rm -f GeoLite2-ASN.mmdb
[ ! -e ./ASN.mmdb ]||rm -f ASN.mmdb
[ ! -e ./whiteiplist.conf ]||rm -f whiteiplist.conf

wget http://geod.01766.cc/whiteiplist.conf
wget http://geod.01766.cc/GeoLite2-Country.mmdb
wget http://geod.01766.cc/GeoLite2-ASN.mmdb
wget http://geod.01766.cc/ASN.mmdb

oldwhiteiplistmd5=`md5sum /usr/local/openresty/nginx/conf/whiteiplist.conf |awk '{print $1}'`
newwhiteiplistmd5=`md5sum whiteiplist.conf|awk '{print $1}'`


oldgeodbmd5=`md5sum /usr/local/openresty/nginx/geoip/GeoLite2-Country.mmdb |awk '{print $1}'`
newgeodbmd5=`md5sum GeoLite2-Country.mmdb |awk '{print $1}'`

oldasndbmd5=`md5sum /usr/local/openresty/nginx/geoip/GeoLite2-ASN.mmdb |awk '{print $1}'`
newasndbmd5=`md5sum GeoLite2-ASN.mmdb |awk '{print $1}'`

oldASNmd5=`md5sum /usr/local/openresty/nginx/geoip/ASN.mmdb |awk '{print $1}'`
newASNmd5=`md5sum ASN.mmdb |awk '{print $1}'`


#比对新旧MD5不一样就更新ip库
if [ "${oldwhiteiplistmd5}" != "${newwhiteiplistmd5}" ];then
   alias mv='mv'
   mv -f whiteiplist.conf /usr/local/openresty/nginx/conf/
fi

if [ "${oldasndbmd5}" != "${newasndbmd5}" ];then
   alias mv='mv'
   mv -f GeoLite2-ASN.mmdb /usr/local/openresty/nginx/geoip/
fi

if [ "${oldASNmd5}" != "${newASNmd5}" ];then
   alias mv='mv'
   mv -f ASN.mmdb /usr/local/openresty/nginx/geoip/
fi

if [ "${oldgeodbmd5}" != "${newgeodbmd5}" ];then
   alias mv='mv'
   mv -f GeoLite2-Country.mmdb /usr/local/openresty/nginx/geoip/
   nginx -t
   if [ $? -eq 0 ];then
      nginx -s reload
   fi
fi
EOF

#check nginx
cat >/opt/check_nginx.sh<<\EOF
#!/bin/bash
openresty_port=`netstat -tlnp|grep 'nginx:'|grep 80|wc -l`
if [ "$openresty_port" == "0" ];then
      /usr/local/openresty/nginx/sbin/nginx
fi
EOF

chmod +x /opt/check_nginx.sh


#定时任务
cat>>/var/spool/cron/root<<\EOF
#每天 11:55 更新geoip库
55 10 * * * sh /opt/upgeoipdb.sh

#nginx检查
* * * * *　ps ax|grep -v grep|grep check_nginx.sh||(sh /opt/check_nginx.sh &)

#清理日志
00 19 */2 * *  for i in `find /home/wwwlog/ -type f -name "*.log"`;do echo >$i;done
00 18 * * * for i in `ls -1 /usr/local/openresty/nginx/logs/*.log`;do echo>$i;done

EOF


#clear history
echo > /root/.bash_history
history -c

