# 服务端机器
# 可以访问公网的机器

# 1，安装nginx和proxy_connect_module模块
https://github.com/chobits/ngx_http_proxy_connect_module


# 2，配置nginx
```
 server {
     listen                         8899;

     resolver                       114.114.114.114 8.8.8.8;

     proxy_connect;
     proxy_connect_allow            80 443 563;
     proxy_connect_connect_timeout  10s;
     proxy_connect_read_timeout     10s;
     proxy_connect_send_timeout     10s;

     location / {
         proxy_pass http://$host;
         proxy_set_header Host $host;
     }
 }
```


# 需要上公网的内网机器配置（本地PC电脑）

# 上公网走代理
```
export http_proxy=http://192.168.0.10:8899
export https_proxy=http://192.168.0.10:8899
```
