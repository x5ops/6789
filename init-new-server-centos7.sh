#!/bin/bash
#系统初始化 2021-07-13

ssh_num=`grep 'UseDNS no' /etc/ssh/sshd_config|wc -l`

if [ "$ssh_num" != "1" ];then
 sed -i '121a UseDNS no' /etc/ssh/sshd_config
fi

sed -i 's/GSSAPIAuthentication yes/GSSAPIAuthentication no/g' /etc/ssh/sshd_config
sed -i 's/#MaxSessions 10/MaxSessions 50/g' /etc/ssh/sshd_config
systemctl restart sshd || service sshd restart


#修改密钥
[ -e /root/.ssh ]||mkdir /root/.ssh/
chmod 700 /root/.ssh
cat >>/root/.ssh/authorized_keys<<EOF
ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAzAbyhTiNQ3blmXPtY8XhUO5JctwbGk4BMBD+8Tussj4MyAD8Pquw5VixhwkE05636qT7bCNzbZGzOPPiQ/JqGr8qX5ldBndruoE268q5D5h29rtX08CRu6sOaVft+XMWBFR8kbgr8I7UQlzgbB==
EOF
chmod 600 /root/.ssh/authorized_keys


#centos
echo 'k888$#%^.com' | passwd --stdin root
wget https://ops-aiops.oss-cn-hongkong.aliyuncs.com/package/iftop-1.0-0.21.pre4.el7.x86_64.rpm
yum install iftop-1.0-0.21.pre4.el7.x86_64.rpm -y
yum install ipset unzip -y

#关闭selinux
sed -i s#SELINUX=enforcing#SELINUX=disabled#g /etc/selinux/config
setenforce 0

#打开开机启动
chmod +x /etc/rc.d/rc.local
echo '/usr/sbin/ipset create sshblacklist hash:ip maxelem 1000000 timeout 86400' >>/etc/rc.d/rc.local
echo 'iptables -I INPUT -m set --match-set sshblacklist src -j DROP' >>/etc/rc.d/rc.local

#设置ssh ipset
/usr/sbin/ipset create sshblacklist hash:ip maxelem 1000000 timeout 86400



#修改时区
#timedatectl set-timezone Asia/Shanghai
ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
sed -i '/MAILTO=root/a CRON_TZ=Asia/Shanghai' /etc/crontab


#优化内核
cat>/etc/sysctl.conf<<EOF
fs.file-max = 655350
kernel.core_uses_pid = 1
kernel.msgmnb = 65536
kernel.msgmax = 65536
kernel.shmmax = 68719476736
kernel.shmall = 4294967296
net.core.wmem_default = 8388608
net.core.rmem_default = 8388608
net.core.rmem_max = 16777216
net.core.wmem_max = 16777216
net.core.somaxconn = 65535
net.core.netdev_max_backlog = 262144
net.ipv4.tcp_max_syn_backlog = 262144
net.ipv4.tcp_max_tw_buckets = 6000
net.ipv4.tcp_sack = 1
net.ipv4.tcp_window_scaling = 1
net.ipv4.tcp_rmem = 4096 4194394 16777216
net.ipv4.tcp_wmem = 4096 4194394 16777216
net.ipv4.udp_mem = 8388608 12582912 16777216
net.ipv4.tcp_fin_timeout = 60
net.ipv4.tcp_synack_retries = 3
net.ipv4.tcp_syn_retries = 1
net.ipv4.tcp_tw_reuse = 0
net.ipv4.ip_forward = 0
net.ipv4.conf.all.send_redirects = 0
net.ipv4.conf.default.send_redirects = 0
net.ipv4.conf.all.rp_filter = 1
net.ipv4.conf.default.rp_filter = 1
net.ipv4.icmp_echo_ignore_broadcasts = 1
net.ipv4.icmp_ignore_bogus_error_responses = 1
net.ipv4.tcp_syncookies = 1
net.ipv4.tcp_mem = 94500000 915000000 927000000
net.ipv4.tcp_max_orphans = 3276800
net.ipv4.ip_local_port_range = 1024 65000
net.nf_conntrack_max = 6553500
net.netfilter.nf_conntrack_max = 6553500
net.netfilter.nf_conntrack_tcp_timeout_close_wait = 60
net.netfilter.nf_conntrack_tcp_timeout_fin_wait = 120
net.netfilter.nf_conntrack_tcp_timeout_time_wait = 120
net.netfilter.nf_conntrack_tcp_timeout_established = 3600
net.ipv6.conf.all.disable_ipv6 = 1
kernel.sysrq = 1
kernel.printk = 5
kernel.threads-max = 655350
kernel.pid_max = 655350
vm.max_map_count = 655350
net.ipv4.tcp_timestamps = 1
net.ipv4.tcp_tw_recycle = 0
vm.swappiness = 10
EOF

#修改句柄limit
cp /etc/security/limits.conf /etc/security/limits.conf.old
cat>/etc/security/limits.conf<<EOF
* soft nofile 655350
* hard nofile 655350
* soft nproc 655350
* hard nproc 655350
* soft core 655350
* hard core 655350
* hard memlock unlimited
* soft memlock unlimited
EOF


#增加 history 显示用户和时间,系统编码
cat >>/etc/profile<<\EOF
#export LANG=en_US.UTF-8
HISTFILESIZE=2000
HISTSIZE=2000
USER_IP=`who -u am i 2>/dev/null| awk '{print $NF}'|sed -e 's/[()]//g'`
export HISTTIMEFORMAT="[%F %T][`whoami`][${USER_IP}] "
EOF
source /etc/profile

#完整主机名和全路径显示
cat>>/root/.bashrc<<\EOF
export PS1='[\u@\H \w]\$ '
EOF

source /root/.bashrc


#关闭firewalld 防火墙
systemctl stop firewalld
systemctl disable firewalld


#防爆力破解ssh,[封禁5分钟内ssh失败大于3次的ip]

cat>/opt/sshdeny.sh<<\EOF
#!/bin/bash
#大于等于3次
DEFINE="3"
cat /var/log/secure|awk '/Failed/{print $(NF-3)}'|sort|uniq -c|awk '{print $2"="$1;}' > /tmp/black.list
for i in `cat /tmp/black.list`
do
  IP=`echo $i |awk -F= '{print $1}'`
  NUM=`echo $i|awk -F= '{print $2}'`
  if [ ${NUM} -ge ${DEFINE} ]; then
  echo "sshd:$IP:deny" >> /etc/hosts.deny
  fi
done
echo > /var/log/secure
rm -f /tmp/black.list
EOF

#centos
yum install ntpdate sysstat -y
#下载zabbix脚本
yum install wget nc -y
wget https://ops-aiops.oss-cn-hongkong.aliyuncs.com/script/zabbix-agent-install2.sh
wget http://fr2.rpmfind.net/linux/centos/7.9.2009/os/x86_64/Packages/mtr-0.85-7.el7.x86_64.rpm
yum install mtr-0.85-7.el7.x86_64.rpm -y

cat>>/var/spool/cron/root<<\EOF
#时间同步
00 * * * * /usr/sbin/ntpdate ntp.aliyun.com||/usr/sbin/ntpdate time.windows.com
EOF

iptables -I INPUT -m set --match-set sshblacklist src -j DROP
sed -i '/sshd/c /usr/sbin/ipset add sshblacklist $IP timeout 86400' /opt/sshdeny.sh
chattr +i /opt/sshdeny.sh

echo -e '\n#封禁6分钟内ssh失败3次的ip\n*/6 * * * * root sh /opt/sshdeny.sh > /dev/null 2>&1' >> /etc/crontab
echo -e '\n#每天23:50解封ssh失败ip\n50 23 * * * root sed -i /^sshd:/d /etc/hosts.deny'>> /etc/crontab

#clear history
echo > /root/.bash_history
history -c

